package com.kshrd.dataaccess001.configuration;


import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;


@Deprecated
//@Configuration
public class JdbcConfiguration {
    @Bean
    public DataSource myPostgresDb(){
        DataSourceBuilder dataSource = DataSourceBuilder.create();
        dataSource.driverClassName("org.postgresql.Driver");
        dataSource.url("jdbc:postgresql://localhost:5432/testdb");
        dataSource.username("postgres");
        dataSource.password("password");
    return dataSource.build();


    }




}
