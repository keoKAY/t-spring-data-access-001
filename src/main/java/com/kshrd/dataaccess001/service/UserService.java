package com.kshrd.dataaccess001.service;

import com.kshrd.dataaccess001.model.User;

import java.util.List;

public interface UserService {

    List<User> findAllUsers();
}
