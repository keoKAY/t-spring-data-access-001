package com.kshrd.dataaccess001.controller;


import com.kshrd.dataaccess001.model.User;
import com.kshrd.dataaccess001.repository.UserRepository;
import com.kshrd.dataaccess001.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller

@Slf4j
public class UserController {

private final UserRepository userRepository;

@Autowired
public  UserController( UserRepository userRepository){

    this.userRepository = userRepository;
}

@GetMapping()
     public String index(Model model){
    List<User> users = new ArrayList<>();

    userRepository.findAll().forEach(i->users.add(i));

    model.addAttribute("users",users);
//    System.out.println("Here is the value of the users ");
//    users.stream().forEach(System.out::println);
    User  resultUser = userRepository.findUserById(1);

    System.out.println("Here is the selected value : "+ resultUser);
    model.addAttribute("findone",resultUser);


    // Working on inserting the new data



    return "index" ;
}


@GetMapping("/update-user")
public String findById(  Model model){

    User updatedUser = new User(5,"Chhab Tharin","M","Hi. I teach web & android in advanced course.Please study hard for your own sake.");
     User resultUser = userRepository.save(updatedUser);

    System.out.println("Resulted User : "+ resultUser);


    return "index";
}
}
