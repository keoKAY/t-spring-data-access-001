package com.kshrd.dataaccess001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataAccess001Application {

    public static void main(String[] args) {
        SpringApplication.run(DataAccess001Application.class, args);
    }

}
