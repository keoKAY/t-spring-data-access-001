package com.kshrd.dataaccess001.repository.repositoryimp;

import com.kshrd.dataaccess001.model.User;
import com.kshrd.dataaccess001.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;


@Repository
public class UserRepoImp implements UserRepository {



    private JdbcTemplate jdbc;

    @Autowired
    UserRepoImp(JdbcTemplate jdbc){
        this.jdbc = jdbc;
    }
    @Override
    public Iterable<User> findAll() {
        return jdbc.query("select * from students",this::mapRowToUser);
    }

    @Override
    public User findUserById(int id) {
        return jdbc.queryForObject("select * from students where id =?",this::mapRowToUser,id);
    }

    @Override
    public User save(User user) {
        jdbc.update("insert into students(id, username,gender, bio) values (?,?,?,?)",
                user.getId(),user.getUsername(),user.getGender(),user.getBio()
                );


        return user;

    }


    private User mapRowToUser(ResultSet rs , int rowNum) throws SQLException {
        return  new User(
                rs.getInt("id"), rs.getString("username"),rs.getString("gender"),
                rs.getString("bio")
        );

    }
}
