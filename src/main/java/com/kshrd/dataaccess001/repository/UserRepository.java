package com.kshrd.dataaccess001.repository;

import com.kshrd.dataaccess001.model.User;
import org.springframework.stereotype.Repository;



public  interface UserRepository {


    Iterable<User> findAll();
    User findUserById(int id ) ;

    User save(User user );
}